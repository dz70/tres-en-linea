export default class Game {
    constructor(){
        console.log('init')
        this.turno = 'x'
        this.board = new Array(9).fill(null)
    }

    // metodo que define el turno de X o Y
    nextTurn(){
        if ( this.turno == 'x' ){
            this.turno = 'o'
        }else {
            this.turno = 'x'
        }
    }

    // metodo que simula el "mapeo" realizado al 
    // momento que alguien hace un movimiento.
    enMovimiento( i ) {

        if( this.juegoTerminado() ){
            return
        }

        if ( this.board[i] ){
            return
        }
        this.board[i] = this.turno
        let ganador = this.combinacionGanadora()
        if( !ganador ){
            this.nextTurn()
        }
    }

    //metodo que compara las combinaciones ganadores
    // con lo que esta siendo seleccionado en el tablero
    combinacionGanadora(){
        const combGanadora = [
            [0,1,2],
            [3,4,5],
            [6,7,8],
            [0,3,6],
            [1,4,7],
            [2,5,8],
            [0,4,8],
            [6,4,2]
        ]

        for( const comb of combGanadora ){
            const [a,b,c] = comb
            if ( this.board[a] && (this.board[a] === this.board[b] && this.board[a] && this.board[c])) {
                return comb
            }
        }
        return null
    }

    juegoTerminado(){
        let combGanadora = this.combinacionGanadora()
        if ( combGanadora ){
            return true
        }else {
            return false
        }
    }

}