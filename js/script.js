import Game from './game.js'
import GameView from './gameView.js'

let game = new Game()
let gameView = new GameView()
gameView.updateBoard(game)

let espacios = document.querySelectorAll('.board-space')

espacios.forEach(espacio => {
    espacio.addEventListener('click', ()=>{
        espacioClic(espacio.dataset.index)
    })
})

function espacioClic( i ){
    game.enMovimiento( i )
    
    gameView.updateBoard( game )
}