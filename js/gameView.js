export default class GameView {
    constructor(){
        console.log('init gv')
    }

    updateBoard(game) {
        for ( let i = 0; i < game.board.length; i++) {
            const espacio = document.querySelector(`.board-space[data-index='${i}']`)
            espacio.textContent = game.board[i]
        }
    }
}